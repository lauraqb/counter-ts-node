import {Handler} from 'aws-lambda'
import {getQuantity} from './src/app'

export const handler: Handler = async (event: any) => {
  if (event && event.action) {
    const action = event.action
    console.log('Action:', action)

    if (action == 'get') console.log('action get')

    return {
      statusCode: 200,
      body: 'hola mundo',
    }
  } else {
    console.error('El evento no tiene una propiedad "action".')
    return {
      statusCode: 400,
      body: 'Bad Request',
    }
  }
}
